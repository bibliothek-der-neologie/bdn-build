#!/bin/bash
# test to parse XQSuite
# for details see http://exist-db.org/exist/apps/doc/xqsuite.xml
#
# everything should end with no failures.
# the post-install.xq - responsible to execute the tests under certain
# conditions, writes a "fail" in the resulting documents.

TEST=$(grep --invert-match 'failures="0"' build/sade_job-*.xml)
TEST=$(grep --no-filename --only-matching --extended-regexp \
        "failures=\"[0-9]+\"" build/sade_job-*.xml \
      | grep --only-matching --extended-regexp "[0-9]+" \
      | paste -sd+ \
      | bc )
if [ "$TEST" -ne "1" ]
  then
    echo "there are failing tests."; exit 1
  else
    echo "no failures found. good."; exit 0
fi
