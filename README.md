[![pipeline status](https://gitlab.gwdg.de/bibliothek-der-neologie/bdn-build/badges/develop/pipeline.svg)](https://gitlab.gwdg.de/bibliothek-der-neologie/bdn-build/commits/develop)

# SADE build targets

This repo contains build scripts for complete SADE instances of the DFG-funded project ["Bibliothek der Neologie"](https://www.bdn-editio.de).

The task is to collect all sources or builds and include them in a single
artifact `build/sade-VERSION.tar.gz`. All other artifacts are available as well.

# Requirements

`ant`

# Use!

Call `ant -f neologie-${BRANCH}.xml` at the root folder should result in a deployable
instance within the `./build/`. Please note that `ant -f neologie-master.xml` triggers building the productive instance while `ant -f neologie-dev.xml` create the developer instance.
There is a tarball available but for local tests you can simply startup `./build/sade/bin/startup.sh` after build is completed.


# Deploy!
There are Debain packages available at the [DARIAH-DE Aptly Repo](https://ci.de.dariah.eu/packages/pool/snapshots/s/sade/), published for
*all* distributions.
